import { Component, OnInit } from '@angular/core';

import { Currency } from './currency';
import { ExchangeService } from './exchange.service';

@Component({
  selector: 'exchange-root',
  templateUrl: './exchange.component.html',
  styleUrls: ['./app.component.scss']
})
export class ExchangeComponent implements OnInit {
  currencies: Currency[] = [];
  quickLinks: string[] = ["KRW", "JPY", "CNY", "HKD", "EUR", "GBP"];
  selectedCurrency: string = "KRW";
  selectedPrice: number = 0.00;

  constructor(private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    this.exchangeService.getCurrencies()
      .then(currencies => this.currencies = currencies);

    this.update(this.selectedCurrency);
  }

  update(currency: string): void {
    this.exchangeService.getLatest(currency)
      .then(price => this.selectedPrice = price);
  }

  onChange(value: string): void {
    this.selectedCurrency = value;
    this.update(this.selectedCurrency);
  }
}
