import { Injectable }    from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

import { Currency } from './currency';
import { environment } from '../environments/environment';

@Injectable()
export class ExchangeService {
	readonly LATEST_URL: string = "https://openexchangerates.org/api/latest.json";
	readonly CURRENCIES_URL: string = "https://openexchangerates.org/api/currencies.json";

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  getLatest(currency: string): Promise<number> {
    const url = `${this.LATEST_URL}?app_id=${environment.exchangeAppId}`;
    return this.http.get(url)
      .toPromise()
      .then(response => {
        const { rates: { [currency]: price } } = response.json();
        return price;
      })
      .catch(this.handleError);
  }

  getCurrencies(): Promise<Currency[]> {
    const url = `${this.CURRENCIES_URL}?app_id=${environment.exchangeAppId}`;
    return this.http.get(url)
      .toPromise()
      .then(response => {
        const data = response.json();
        let currencies: Currency[] = [];
        for (let k in data) {
          currencies.push({ code: k, name: data[k] });
        }
        return currencies;
      })
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}